package Servlet.accesoServicios;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Servicio.ServicioComunicacion;

public class ObtenerVariables  extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;




	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		processRequest(req, res);
	}
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		processRequest(req, res);
	}
	
	
	
	
	public void processRequest(HttpServletRequest req,HttpServletResponse res) throws IOException
	{
		
		/*
		 * obtener el valor de la sesion de usuario
		 * */
		HttpSession session = req.getSession(true);
		String usuario=session.getAttribute("usuario").toString();/*obtenido de la sesion*/

		String contrasena=session.getAttribute("contrasena").toString();/*obtenido de la sesion*/
		res.getWriter().write(ServicioComunicacion.obtenerVariables(usuario, contrasena).toString());
		
		
	}
	
}
