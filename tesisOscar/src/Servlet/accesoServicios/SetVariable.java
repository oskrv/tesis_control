package Servlet.accesoServicios;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Servicio.ServicioComunicacion;

public class SetVariable extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
{
	
		/*
		 * obtener el valor de la sesion de usuario
		 * */
		int clave=	Integer.parseInt(req.getParameter("clave").toString());
		String valor=req.getParameter("valor").toString();
		
		HttpSession session = req.getSession(true);
		String usuario=session.getAttribute("usuario").toString();/*obtenido de la sesion*/

		String contrasena=session.getAttribute("contrasena").toString();/*obtenido de la sesion*/
		System.out.print("clave:"+clave+", valor:"+valor);
		ServicioComunicacion.escribirVariable(clave, valor, usuario, contrasena);
		
		System.out.print("entre");
}
	
	
	

}
