package Servicio.comunicacion.generador;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import javax.imageio.stream.FileImageInputStream;

public class UtilLectorComandos {

	public static ArrayList<String> cargarComandosLectura()
	{	ArrayList<String> salida=new ArrayList<String>();
		Properties properties=new Properties();
		try {
			//properties.load(new FileInputStream("../inf/comandos.properties"));
			properties.load(new FileInputStream("D:/Users/Usuario/Desktop/inf/comandos.properties"));
			//properties.load(new FileInputStream("/home/pi/Desktop/inf/comandos.properties"));
			//properties.load(new FileInputStream("C:/Users/andres/Desktop/inf/comandos.properties"));
			
			System.out.println("comandos.properties encontrado");
			String cadenaComandos=properties.getProperty("lectura");
			
			
			String claves[]=cadenaComandos.split(",");
			for(String clave:claves)
			{
				salida.add(clave);
			}
			return salida;
			
		} catch (FileNotFoundException e) {
		
			System.out.println("archivo no encontrado");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.print("error IO!!!!");
		}
		return null;
		
	}
	
	
	
	public static ArrayList<String> cargarComandosEscritura()
	{	ArrayList<String> salida=new ArrayList<String>();
		Properties properties=new Properties();
		try {
			//properties.load(new FileInputStream("../inf/comandos.properties"));
			//properties.load(new FileInputStream("D:/Users/Usuario/Desktop/inf/comandos.properties"));
			properties.load(new FileInputStream("C:/Users/andres/Desktop/inf/comandos.properties"));
			
			System.out.println("comandos.properties encontrado");
			String cadenaComandos=properties.getProperty("escritura");
			
			
			String claves[]=cadenaComandos.split(",");
			for(String clave:claves)
			{
				salida.add(clave);
				
			}
			return salida;
			
		} catch (FileNotFoundException e) {
		
			System.out.println("archivo no encontrado");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.print("error IO!!!!");
		}
		return null;
		
	}
	
	
	
}
