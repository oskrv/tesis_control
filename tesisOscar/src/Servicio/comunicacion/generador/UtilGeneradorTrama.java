package Servicio.comunicacion.generador;

import java.util.ArrayList;
/*
 * Esta clase genera las tramas Para la escritura y lectura de variable, que seran enviadas por comunicacion serial
 * */
public class UtilGeneradorTrama {

	ArrayList<String> listaComandosEscritura;
	ArrayList<String> listaComandosLectura;
	
	/*
	 * 
	 * los parametros para el constructor son la lista de comandos de lectura y la lsita de comando de escritura
	 * */
	public UtilGeneradorTrama(ArrayList<String> listaComandosLectura,ArrayList<String> listaComandosEscritura)
	{
		this.listaComandosLectura=listaComandosLectura;
		this.listaComandosEscritura=listaComandosEscritura;/**/
		
	}
	
	/*
	 * 
	 * getters y setters
	 * */
	
	public ArrayList<String> getListaComandosEscritura() {
		return listaComandosEscritura;
	}



	public void setListaComandosEscritura(ArrayList<String> listaComandosEscritura) {
		this.listaComandosEscritura = listaComandosEscritura;
	}



	public ArrayList<String> getListaComandosLectura() {
		return listaComandosLectura;
	}
	public void setListaComandosLectura(ArrayList<String> listaComandos) {
		this.listaComandosLectura = listaComandos;
	}
	
	public int logitudListaComandosLectura()
	{
		
		return this.listaComandosLectura.size();
	}
	
	public int logitudListaComandosEscritura()
	{
		
		return this.listaComandosEscritura.size();
	}
	/*
	 * fin  de los getters y setters
	 * */
	
/*
 * 
 * Busca en la lista de comandos de lectura el comando en la posicion claveComando y retorna el comando
 * */
	public String generarTramaLectura(int claveComando)
	{
		if(claveComando<this.logitudListaComandosLectura())
			return listaComandosLectura.get(claveComando);
		else
			{System.out.println("clave invalida");
			return null;
			}		
	}
	/*
	 * Busca el comando en la lista a partir de la clave y le agrega : valor. retornando esa trama
	 * */
	public String generarTramaEscritura(int claveComando,String valor)
	{
		if(claveComando<this.logitudListaComandosEscritura())
			return listaComandosEscritura.get(claveComando)+":"+valor;
		else
			{System.out.println("clave invalida");
			return null;
			}		
	}
	
}
