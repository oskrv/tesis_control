package Servicio.comunicacion.serial;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;


public class SerialUitility {

	static String portname;
	static String baudrate;
	static String databits;
	static SerialPort serialPort;
	static OutputStream out;
	static InputStream in;
	static Boolean estado=false;
	public static void init()
	{
		Properties properties=new Properties();
		try {
			//properties.load(new FileInputStream("C:/Users/andres/Desktop/inf/serial.properties"));
			//properties.load(new FileInputStream("../inf/serial.properties"));
			properties.load(new FileInputStream("D:/Users/Usuario/Desktop/inf/serial.properties"));
			//properties.load(new FileInputStream("/home/pi/Desktop/Desktop/inf/serial.properties"));
			
			
			portname=properties.getProperty("portName");
			baudrate=properties.getProperty("baudRate");
			databits=properties.getProperty("dataBits");
			System.out.println("lectura de serial.properties exitosa");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("archivo serial.properties no encontrado");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public static void open()
	{
		if(!estado){
        CommPortIdentifier portIdentifier;
		try {
			portIdentifier = CommPortIdentifier.getPortIdentifier(portname);
			
			CommPort commPort = portIdentifier.open("rxtx", 2000);
	        
	        serialPort 	=	 (SerialPort) commPort;
	        serialPort.setSerialPortParams(Integer.parseInt(baudrate),Integer.parseInt(databits),SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
	        out 		=	 serialPort.getOutputStream();
	        in =serialPort.getInputStream();
	        estado		=	 true;
		} catch (NoSuchPortException | PortInUseException | NumberFormatException | UnsupportedCommOperationException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		
		
		
		}
	}
	public static void close()
	{
		if(estado)
		{
			serialPort.close();
			estado=false;
		}
	}
	
	public static OutputStream getOutputStream()
	{
		
		return out;
	}
	
	public static InputStream getInputStream()
	{
		
		return in;
	}
}
