package Servicio.comunicacion.serial;

import java.io.IOException;
import java.io.InputStream;


public class SerialReader implements Runnable{

	
	  InputStream in;
      String cadenamensaje;
      Boolean mensajeRecibido;
      
      ErrorTemporizador tempErr=new ErrorTemporizador();
      long tiempo=5000;
      
      Temporizador temporizador=new Temporizador(tempErr, tiempo);
      Thread hiloTiempo=new Thread(temporizador);
      
      
      
      
      public SerialReader ( InputStream in )
      {
          this.in = in;
          cadenamensaje=new String();
          mensajeRecibido=false;
      }
      
      @SuppressWarnings("deprecation")
	public void run ()
      {		hiloTiempo.start();
          byte[] buffer = new byte[1024];
          int len = -1;
          try
          {
        	  System.out.println("leyendo");
              while (mensajeRecibido==false&&tempErr.getErr()==false)
              {  int caracter=this.in.read();
            	  if(caracter!='/')
            		  {if(caracter>-1)
            			  {this.cadenamensaje=cadenamensaje+(char)caracter;
            		  	  hiloTiempo.stop();}
            		  
            		  }
            	  else
            	  {
            		  mensajeRecibido=true;
            		  
            		  
            	  }
            	  Thread.sleep(10);
            	}
        	 if(tempErr.getErr())
        	 {
        		 System.out.println("error no se recibio ninguna respuesta");
        		 this.cadenamensaje=null;
        		 mensajeRecibido=true;
        	 }
              
          }
          catch ( IOException | InterruptedException e )
          {
              e.printStackTrace();
          } 
        
      }
      
      
      public String getMensajeRecibido()
      {	String ret=cadenamensaje;
      	this.cadenamensaje=new String();
      	mensajeRecibido=false;
    	return ret;
      }
      
      
      public boolean isMenajeRecibido()
      {
    	  return this.mensajeRecibido;
      }
      
}
