package Servicio.comunicacion.variable;

public class Variable {
	String nombre;
	double valor;
	/*
	 * constructores
	 * */
	public Variable()
	{
	nombre="desconocido";
	valor=0;
	}
	
	public Variable(String nombre)
	{
	this.nombre	=	nombre;	
	}
	
	public Variable(String nombre,double valor)
	{
		
		this.nombre=nombre;
		this.valor=valor;
	}
	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}
	public String toString()
	{
		return this.nombre+":"+this.valor;
		
	}
	public Variable clone()
	{
		Variable clonada=new Variable(this.nombre,this.valor);
		return clonada;
	}
	
	
	
	
}
