package Servicio.comunicacion.variable;

import java.util.ArrayList;

public class ListaVariables extends ArrayList<Variable>


{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public boolean existeVariable(String nombre)
	{
		for (Variable var :this)
		{
			if(var.getNombre().equals(nombre))
				return true;
		}	
		return false;
	}
	
	public Variable getVariable(String nombre)
	{
		for (Variable var :this)
		{
			if(var.getNombre().equals(nombre))
				return var;
		}	

		return null;
		
	}
	
	/*
	 * retorna verdadero si crea la variable
	 * retorna false si la actualiza
	 * */
	public boolean agregarOActualizarVariable(Variable var)
	{
		if(existeVariable(var.getNombre()))
		{
			this.getVariable(var.getNombre()).setValor(var.getValor());
			return false;
		}
		else
		{
			this.add(var);
			return true;
		}
		
	}
	/*
	 * retorna verdadero si exisitia la variable y la removio
	 * retorna false s no existia y no se hizo nada
	 * */
	public boolean removerVariable(String nombre)
	{
		if(existeVariable(nombre))
		{
			this.remove(this.getVariable(nombre));
			return true;
		}
		else
			return false;
	}
	
	
	
}
