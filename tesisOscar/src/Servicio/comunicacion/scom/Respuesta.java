package Servicio.comunicacion.scom;

import Servicio.comunicacion.variable.ListaVariables;
import Servicio.comunicacion.variable.Variable;

public class Respuesta {
	private String contenido;
	private ListaVariables lista;
	
	
	
	public Respuesta(){}
	
	public Respuesta(ListaVariables lista)
	{
		this.lista=lista;
	}
	public Respuesta (ListaVariables lista,String contenido)
	{
		
		this.contenido=contenido;
		this.lista=lista;
	}
	
	
	public String getContenido() {
		return contenido;
	}







	public void setContenido(String contenido) {
		this.contenido = contenido;
	}







	public ListaVariables getLista() {
		return lista;
	}







	public void setLista(ListaVariables lista) {
		this.lista = lista;
	}







	public Boolean ProcesarContenido()
	{
		if (contenido==null)
		{
			return false;
		}
		String[] cadenas=contenido.split(",");
		for(String cadena:cadenas)
		{
		String[] subCadena=cadena.split(":");
		Variable varAux=new Variable(subCadena[0],Double.parseDouble(subCadena[1]));
		lista.agregarOActualizarVariable(varAux);
		}
		
		return true;
	}
	
	
}
