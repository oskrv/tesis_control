package Servicio.comunicacion.scom;

import java.io.InputStream;
import java.io.OutputStream;

import Servicio.comunicacion.serial.SerialReader;
import Servicio.comunicacion.serial.SerialWriter;
import Servicio.comunicacion.variable.ListaVariables;


public class Lectura implements Mensaje {
	

	private ListaVariables lista;
	private String trama;
	private Respuesta resp;
	private OutputStream out;
	private InputStream in;
	
	public Lectura(ListaVariables lista,InputStream in,OutputStream out)
	{
		this.lista=lista;
		this.out=out;
		this.in=in;
		/*
		 * 
		 * no se si usar intputStreamReader o solo inputStream
		 * verificar
		 * */
	}
	
	
	public String getTrama() {
		return trama;
	}

	public void setTrama(String trama) {
		this.trama = trama;
	}

	public Lectura(ListaVariables lista) {
	
		this.lista=lista;
		setResp(new Respuesta(lista));
	// TODO Auto-generated constructor stub

		
	}
	
	public ListaVariables getLista() 
	{

		return lista;
	}

	public void setLista(ListaVariables lista)
	{
		
		this.lista = lista;
		setResp(new Respuesta(lista));
	}

	
	
	
	@Override
	public boolean ejecutar() {
		// TODO Auto-generated method stub
		SerialWriter escritura =new SerialWriter(out);
		SerialReader lectura= new SerialReader(in);
		
		escritura.SetMensaje(trama);
		Thread lect=new Thread(lectura);
		lect.start();
		new Thread(escritura).start();
	
		while(lect.isAlive()){}
		//System.out.print(lectura.getMensajeRecibido());/*borrar*/
		Respuesta resp=new Respuesta(lista, lectura.getMensajeRecibido());
		resp.ProcesarContenido();
		return true;
	}


	public Respuesta getResp() {
		return resp;
	}


	public void setResp(Respuesta resp) {
		this.resp = resp;
	}
	

}
