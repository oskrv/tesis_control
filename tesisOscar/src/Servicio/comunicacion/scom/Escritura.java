package Servicio.comunicacion.scom;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class Escritura implements Mensaje {
	private String trama;
	private OutputStreamWriter out;
	
	public Escritura(OutputStream out)
	{
		this.out=new OutputStreamWriter(out);
		
	}
	
	public String getTrama()
	{
		return trama;
	}

	public void setTrama(String trama) {
		this.trama = trama;
	}

	@Override
	public boolean ejecutar() {
		// TODO Auto-generated method stub
		if(trama!=null)
			try {
				out.write(trama);
				out.flush();
				trama	=	null;
				return true;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Error Al escribir serial");
			}
			
		
		return false;
	}

}
