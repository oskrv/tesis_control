package Servicio;

import Servicio.comunicacion.generador.UtilGeneradorTrama;
import Servicio.comunicacion.generador.UtilLectorComandos;
import Servicio.comunicacion.scom.Escritura;
import Servicio.comunicacion.scom.Lectura;
import Servicio.comunicacion.scom.Mensaje;
import Servicio.comunicacion.serial.SerialUitility;
import Servicio.comunicacion.variable.ListaVariables;
import Servicio.comunicacion.variable.Variable;
import controlador.*;
public class ServicioComunicacion {

	static boolean inicializado=false;
	static boolean ejecutando=false;
	static UtilGeneradorTrama generadorTrama;
	static boolean disponible;
	static ListaVariables listaVariables=new ListaVariables();
	public static boolean  InicioDelServicio()
	{
		if(!inicializado)
			{SerialUitility.init();
			generadorTrama=new UtilGeneradorTrama(UtilLectorComandos.cargarComandosLectura(),UtilLectorComandos.cargarComandosEscritura());
			disponible		=	true;
			inicializado	=	true;
		return true;}
		
		return false;
	}
	
	
	public static boolean ejecutar(String usuario,String contrasena)
	{
		if(new consultas().autenticacion(usuario, contrasena))
		{new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(true)
				{
					if(disponible)
					{
						disponible=false;
						SerialUitility.open();
						BarridoLectura(listaVariables, generadorTrama);
						SerialUitility.close();
						disponible=true;
					}
					
					/*al terminar el barrido espera 0.5 s*/
					try {Thread.sleep(5000);}catch (InterruptedException e) {}
					
				}
				// TODO Auto-generated method stub
				
			}
		}).start();
	
	
		return true;}
		else
			return false;
	}
	
	
	private static void BarridoLectura(ListaVariables listaVariables,UtilGeneradorTrama generadorTrama)
	{	
		int size=generadorTrama.getListaComandosLectura().size();
		for(int i=0;i<size;i++)
		{
			Mensaje lectura=new Lectura(listaVariables,SerialUitility.getInputStream(), SerialUitility.getOutputStream());
			lectura.setTrama(generadorTrama.generarTramaLectura(i));
			System.out.println("trama generada:"+generadorTrama.generarTramaLectura(i));
			/*fin de la inicializacion del mensae*/
			/*ejecucion*/
			lectura.ejecutar();
			
		}
		
	}
	
	public static boolean escribirVariable(final int clave,final String valor,String usuario,String contrasena)
	{	if(new consultas().autenticacion(usuario, contrasena))
	{
		new Thread(new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(!disponible)
			{	/*espera hasta que el puerto este disponible*/
				try {Thread.sleep(1000);} catch (InterruptedException e) {}
			}
			/*ocupa el puerto*/
			disponible=false;
			
			SerialUitility.open();											//abre el puerto
			String trama=generadorTrama.generarTramaEscritura(clave,valor); //generando trama de escritura de variable
			Mensaje esc=new Escritura(SerialUitility.getOutputStream());// creacion del objetoo de escritura e inyeccion del OuputStream en el objeto de escritut
			esc.setTrama(trama);  											//inyeccion de la trama
			esc.ejecutar();													// ejecuccion de la escritura
			SerialUitility.close();											//cierr el puerto
			disponible=true;												// puerto disponible nuevamente
		}
	}).run();
		return true;}
	return false;
	}
	
	
	public static boolean escribirVariable(final String trama,String  usuario,String contrasena)
	{
		if(new consultas().autenticacion(usuario, contrasena))
		{
			
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					while(!disponible)
					{	/*espera hasta que el puerto este disponible*/
						try {Thread.sleep(1000);} catch (InterruptedException e) {}
					}
					/*ocupa el puerto*/
					disponible=false;
					
					SerialUitility.open();											//abre el puerto
					Mensaje esc=new Escritura(SerialUitility.getOutputStream());// creacion del objetoo de escritura e inyeccion del OuputStream en el objeto de escritut
					esc.setTrama(trama);  											//inyeccion de la trama
					esc.ejecutar();													// ejecuccion de la escritura
					SerialUitility.close();											//cierr el puerto
					disponible=true;												// puerto disponible nuevamente
				}
			}).run();
				return true;	
			
			
			
			
		}
		return false;
	}
	public static String obtenerVariables(String usuario,String contrasena)
	{
		String salida="{";
		int cont=0;
		
		for (Variable a:listaVariables)
		{	
			salida=salida+"\""+a.getNombre()+"\":"+a.getValor();
			cont++;
			if(cont<listaVariables.size())
				salida+=",";
		}
		salida=salida+"}";
		return salida;
	}
	
}
