/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;


import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class consultas extends Conexion{
    
    public boolean autenticacion(String usuario, String contrasena){
        PreparedStatement pst = null;
        ResultSet rs = null;
        
        
        try {
            String consulta = "select * from usuarios where usuario = ? and pass = ?";
            pst = getConexion().prepareStatement(consulta);
            pst.setString(1, usuario);
            pst.setString(2, contrasena);
            rs = pst.executeQuery();
            
            if(rs.absolute(1)){
                return true; 
            }
            
        } catch (Exception e) {
            System.err.println("ERROR"+e);
        }finally{
            try {
                if (getConexion() != null) getConexion().close();
                if (pst != null) pst.close();
                if (rs != null) rs.close();
                
            } catch (Exception e) {
                System.err.println("ERROR"+e);
            }
        }
        return false;
    }
    
    /*public static void main(String[] args) {
        consultas co = new consultas();
        System.err.println(co.autenticacion("velasquezoscar","123"));
    }
*/
    
    public boolean registrar(String nombre, String apellido, String usuario, String contrasena, String telefono,String cedula){
    	//public boolean registrar(String usuario, String contrasena, String nombre, String apellido, String cedula,String telefono){
       
        PreparedStatement pst = null;
        
        try {
            String consulta = "insert into usuarios (nombre, apellido, usuario, pass, telefono, cedula) values(?,?,?,?,?,?)";
            pst= getConexion().prepareStatement(consulta);
            pst.setString(1, nombre);
            pst.setString(2, apellido);
            pst.setString(3, usuario);
            pst.setString(4, contrasena);
            pst.setString(5, telefono);
            pst.setString(6, cedula);
            
            //String consulta = "insert into usuario (usuario, pass, nombre, apellido, cedula, telefono) values(?,?,?,?,?,?)";
            //pst= getConexion().prepareStatement(consulta);
            //pst.setString(1, usuario);
            //pst.setString(2, contrasena);
            //pst.setString(3, nombre);
            //pst.setString(4, apellido);
            //pst.setString(5, cedula);
            //pst.setString(6, telefono);
            
            if(pst.executeUpdate()== 1){
                return true;
            }
            
        } catch (Exception ex) {
            System.err.println("ERROR"+ex);
        }finally{
            try {
                    if(getConexion() != null) getConexion().close();
                    if(pst != null) pst.close();
                    
            } catch (Exception e) {
                System.err.println("ERROR"+e);
            }
        }
        return false;
    }
    
}
