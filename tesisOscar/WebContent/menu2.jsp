<%-- 
    Document   : menu2
    Created on : 11/01/2016, 06:47:06 PM
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HttpSession objsesion = request.getSession(false);
    String usuario = (String)objsesion.getAttribute("usuario");
    if (usuario.equals("")){
        response.sendRedirect("inicio.jsp");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <title>Menu de Control</title>
        <script src="js/jquery.js"></script>
        <script src="js/obtenerVariables.js"></script>
        <script src="js/RegulacionTemperatura.js"></script>
        <script src="js/alarmaV2.js"></script>
    </head>
    <body>
        <div id="index">
         <h1 align="left"> Bienvenido  <% out.println(usuario);%> </h1>
         <h1 align="center"> <img src="imagen/cantv.jpg"/></h1>
         <div id="mensaje">
        </div>
        <table align="center">
            <tr>
            <td>
           <a href="menu.jsp"><h2> CONTROL Y SUPERVISION</h2> </a>
            </td>
            </tr>
           <tr>
            <td>
           
          <a href="InicializarServicio"><h2>INICIAR SERVICIO</h2> </a>
            </td>
            </tr>
            <tr>
            <td>
                <a href="RegistroySeguridad.jsp"><h2>Seguridad Y Registro</h2></a>
            </td>
            </tr>
             <tr>
            <td>
                <a href= "inicio.jsp"><h2>CERRAR SESION</h2></a>
            </td>
        </table>
        </div>
        
        
        
        <div class="monPanel" hidden=true>
        
         <!--  sustituir value en id,por ejemplo item_id_nombreVariable.
         dejar class tal como esta es decir item_value.
          hacer esto para todas las variables con class=item_value!-->
        
         <label><b>Sala Digital:</b></label><label id="item_id_t1" class="item_value">???</label><br><br>
         <input type="button" value="ALARMA 1"/><label id="item_id_al1" class="item_value">???</label><br>
         <input type="button" value="ALARMA 2"/><label id="item_id_al2" class="item_value">???</label>
         <br><br><br>
         <label><b>Oficina Movilnet:</b></label><label id="item_id_t1" class="item_value">???</label><br><br>
         <label><b>Sala PCM:</label></b><label id="item_id_t1" class="item_value">???</label><br><br>
         <label><b>Sala Datos:</b></label><label id="item_id_value" class="item_value">???</label><br><br>

         <label><b><font color="black">Temperatura 1:</font></b></label><label id="item_id_temp1" class="item_value">???</label>
         <label><b>Temperatura 2:</b></label><label id="item_id_temp2" class="item_value">???</label>
         <label><b>Temperatura 3:</b></label><label id="item_id_temp3" class="item_value">???</label>
         <label><b>Temperatura 4:</b></label><label id="item_id_temp4" class="item_value">???</label>
         <br><br>
         <label><b>Voltajes En DC:</b></label><label id="item_id_VDC1" class="item_value">???</label>
         <label><b>Voltajes En DC:</b></label><label id="item_id_VDC2" class="item_value">???</label>
         <label><b>Voltajes En DC:</b></label><label id="item_id_VDC3" class="item_value">???</label>
         <label><b>Voltajes En DC:</b></label><label id="item_id_VDC4" class="item_value">???</label>
         <br><br>
         <label><b>Fase 1:</b></label><label id="item_id_VAC1" class="item_value">???</label>
         <label><b>Fase 2:</b></label><label id="item_id_VAC2" class="item_value">???</label>
         <label><b>Fase 3:</b></label><label id="item_id_VAC3" class="item_value">???</label>
         
		</div>
		<div id="alarma">
		        <!-- aqui se definen las alarams formato es <label class="alarm_parameter" id="alarm_nombreVariable_tipoLimite_valorLimite" hidden="true"></label> -->
		        <!-- ALARMAS -->
		        <label class="alarm_parameter" id="alarm_t1_max_26"  hidden=true font color="red"><!-- mensaje de alarma ejemplo-->ALARMA EN MERIDA II</label>
        <label class="alarm_parameter" id="alarm_VDC1_min_44"  hidden=true ><font color="red">bajo voltaje</font></label>
        <label class="alarm_parameter" id="alarm_VDC2_min_44"  hidden=true font color="red">bajo voltaje</label>
        <label class="alarm_parameter" id="alarm_VDC3_min_44"  hidden=true font color="red">bajo voltaje</label>
        <label class="alarm_parameter" id="alarm_VDC4_min_44"  hidden=true font color="red">bajo voltaje</label>
        <label class="alarm_parameter" id="alarm_VAC1_min_90"  hidden=true font color="red">falla de red</label>
        <label class="alarm_parameter" id="alarm_VAC2_min_90"  hidden=true font color="red">falla de red</label>
        <label class="alarm_parameter" id="alarm_VAC3_min_90"  hidden=true font color="red">falla de red</label>
		       		</div>
        
        
    </body>
</html>
