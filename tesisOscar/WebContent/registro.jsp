<%-- 
    Document   : registro
    Created on : 22/12/2015, 10:24:56 AM
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="StyleSheet" href="css/estilo2.css" type="text/css">
        <title>Pagina de Registro</title>
        <script src="js/main2.js"></script>
    </head>
    <body>
        <div id="index">
        <h1 align="center">Registro de Usuarios</h1>
        <form action="nuevousuario" method="post" id="regis">
            <table align="center" >
                <tr>
                    <td><label for="usuario">USUARIO</label></td>
                <td><input type="text" name="usuario" id="usuario"/></td>
                </tr>
                <tr>
                    <td><label for="nombre">NOMBRE</label></td>
                    <td><input type="text" name="nombre" id="nombre"/></td>
                </tr>
                <tr>
                    <td><label for="apellido">APELLIDO</label></td>
                <td><input type="text" name="apellido" id="apellido"/></td>
                </tr>
                <tr>       
                    <td><label for="pass">CONTRASEÑA</label></td>
                    <td><input type="password" name="pass" id="pass"/></td>
                </tr>
                <tr>
                    <td><label for="telefono">TELEFONO</label></td>
                <td><input type="text" name="telefono" id="telefono"/></td>
                </tr>
                <tr>
                    <td><label for="cedula">CEDULA</label></td>
                    <td><input type="text" name="cedula" id="cedula"/></td>
                </tr>
                <tr>
                    <td><input type="button" value="REGISTRAR" id="onregis" /></td>
            </tr>
            </table>
        </form>
        
        <h2 align="center"> <a href="RegistroySeguridad.jsp">ATRAS</a></h2> 
        <br><br>
        
        </div>
    </body>
</html>
