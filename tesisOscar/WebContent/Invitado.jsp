<%-- 
    Document   : menu
    Created on : 22/12/2015, 11:07:58 AM
    Author     : Oscar Velasquez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="StyleSheet" href="css/estilo.css" type="text/css">
       
        <title> SISTEMA DE SUPERVISION y CONTROL</title>
    	<script src="js/jquery.js"></script>
        <script src="js/obtenerVariables.js"></script>
        <script src="js/RegulacionTemperatura.js"></script>
        <script src="js/ManejoRele.js"></script>
        <script src="js/alarmaV2.js"></script>    
        <script src="js/bitdisplay.js"></script>    
    </head>
    <body>
  
        <h1 align="center" >Sistema de Supervision y Control de la Central Merida II</h1>
        <label aling= "Right">Iniciar Sistema</label>
        <input type="button" value="Start"  id=""/>
        
        <div id="indice">
        <h2 align="center"> Temperaturas</h2>
        <div class="monPanel">
        
         <!--  sustituir value en id,por ejemplo item_id_nombreVariable.
         dejar class tal como esta es decir item_value.
          hacer esto para todas las variables con class=item_value!-->
          <!-- <label>PIC termostato</label>-->
        <!--<label id="item_id_E1" class="item_value" hidden=true>???</label><div id="item_bit_E1" class="item_bit_display"  style="height:30px;width:60px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
         -->
        
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><b>Sala Digital:</b></label><label id="item_id_t1" class="item_value">???</label><br><br>
         <input type="button" value="ALARMA 1"/><label id="item_id_al1" class="item_value">???</label><br>
         <input type="button" value="ALARMA 2"/><label id="item_id_al2" class="item_value">???</label>
         
		 <!--  <label><b>Oficina Movilnet:</b></label><label id="item_id_t1" class="item_value">???</label><br><br>
         <label><b>Sala PCM:</label></b><label id="item_id_t1" class="item_value">???</label><br><br>
         <label><b>Sala Datos:</b></label><label id="item_id_value" class="item_value">???</label><br><br>
         -->
         </div> 
        </div>
       <div id="contenido">
         <h2 align="center"> Monitoreo de Variables Ambientales y Voltajes</h2>
        <div class="monPanel">
        <!--  sustituir value en id,por ejemplo item_id_nombreVariable.
         dejar class tal como esta es decir item_value.
          hacer esto para todas las variables con class=item_value!-->
           <h2 align="center"> Temperaturas </h2>
         &nbsp;&nbsp;&nbsp;<label><b><font color="black">Temperatura 1:</font></b></label><label id="item_id_temp1" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b>Temperatura 2:</b></label><label id="item_id_temp2" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b>Temperatura 3:</b></label><label id="item_id_temp3" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b>Temperatura 4:</b></label><label id="item_id_temp4" class="item_value">???</label>
         <br><br>
          <h2 align="center"> Voltajes En DC </h2>
         &nbsp;&nbsp;&nbsp;<label><b>Voltajes En DC 1:</b></label><label id="item_id_VDC1" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b>Voltajes En DC 2:</b></label><label id="item_id_VDC2" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b>Voltajes En DC 3:</b></label><label id="item_id_VDC3" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b>Voltajes En DC 4:</b></label><label id="item_id_VDC4" class="item_value">???</label>
         <br><br>
          <h2 align="center"> Voltajes En AC </h2>
         &nbsp;&nbsp;&nbsp;<label><b>Fase 1:</b></label><label id="item_id_VAC1" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b>Fase 2:</b></label><label id="item_id_VAC2" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b>Fase 3:</b></label><label id="item_id_VAC3" class="item_value">???</label>
         </div>
         
         <h2 align="center"> ENTRADAS DIGITALES </h2>
       <!-- el numero que se encuentra justo despues  de "rele_id_" se debe sustituir por el numero de la posicion del comando en el .properties, y luego del "value_"  se debe colocar el valor que se desea enviar al presionar ese boton (Ej:on,off,1,0,)
       por ejemplo
       	id="rele_id_0_value_on"
       	significa que se envia el comando que esta en la posicion 0 y el valor on
       	id="rele_id_0_value_1"
       	significa que se envia el comando que esta en la posicion 0 y el valor 1
      -->
         <label>Dispositivo 1</label>
         <label id="item_id_E1" class="item_value" hidden=true>???</label><div id="item_bit_E1" class="item_bit_display"  style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
          <label>Dispositivo 2</label>
          <label id="item_id_E2" class="item_value" hidden=true>???</label><div id="item_bit_E2" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
          <label>Dispositivo 3</label>
          <label id="item_id_E3" class="item_value" hidden=true>???</label><div id="item_bit_E3" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
          <br><br>
          <label>Dispositivo 4</label>
          <label id="item_id_E4" class="item_value" hidden=true>???</label><div id="item_bit_E4" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
          <label>Dispositivo 5</label>
          <label id="item_id_E5" class="item_value" hidden=true>???</label><div id="item_bit_E5" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
           <label>Dispositivo 6</label>
          <label id="item_id_E6" class="item_value" hidden=true>???</label><div id="item_bit_E6" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
          <br><br>
           <label>Dispositivo 7</label>
          <label id="item_id_E7" class="item_value" hidden=true>???</label> <div id="item_bit_E7" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
          <label>Dispositivo 8</label>
          <label id="item_id_E8" class="item_value" hidden=true>???</label><div id="item_bit_E8" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
       <h2 align="center"> Control de Reles </h2>
    
        &nbsp;&nbsp;&nbsp;<label>Rele 1</label>
        <input type="button" value=" Encender" id="rele_value_S1" 	 class="rele_button"/>
         <input type="button" value=" Apagar" 	id="rele_value_N1" class="rele_button"/>
         <label id="item_id_R1" class="item_value" hidden=true>???</label><div id="item_bit_R1" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
         <label>Rele 2 </label>
         <input type="button" value=" Encender" id="rele_value_S2" class="rele_button"/>
         <input type="button" value=" Apagar" 	id="rele_value_N2" class="rele_button"/>
         <label id="item_id_R2" class="item_value" hidden=true>???</label><div id="item_bit_R2" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;<br><br>
         &nbsp;&nbsp;&nbsp;<label>Rele 3</label>
         <input type="button" value=" Encender" id="rele_value_S3" class="rele_button"/>
         <input type="button" value=" Apagar" 	id="rele_value_N3" class="rele_button"/>
         <label id="item_id_R3" class="item_value" hidden=true>???</label><div id="item_bit_R3" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
         <label>Rele 4</label>
         <input type="button" value=" Encender" id="rele_value_S4" class="rele_button"/>
         <input type="button" value=" Apagar" 	id="rele_value_N4" class="rele_button"/>
         <label id="item_id_R4" class="item_value" hidden=true>???</label><div id="item_bit_R4" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;<br><br>
         &nbsp;&nbsp;&nbsp;<label>Rele 5</label>
         <input type="button" value=" Encender" id="rele_value_S5" class="rele_button"/>
         <input type="button" value=" Apagar" 	id="rele_value_N5" class="rele_button"/>
         <label id="item_id_R5" class="item_value" hidden=true>???</label><div id="item_bit_R5" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
         <label>Rele 6</label>
         <input type="button" value=" Encender" id="rele_value_S6" class="rele_button"/>
         <input type="button" value=" Apagar" 	id="rele_value_N6" class="rele_button"/>
         <label id="item_id_R6" class="item_value" hidden=true>???</label><div id="item_bit_R6" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;<br><br>
         &nbsp;&nbsp;&nbsp;<label>Rele 7</label>
         <input type="button" value=" Encender" id="rele_value_S7" class="rele_button"/>
         <input type="button" value=" Apagar" 	id="rele_value_N7" class="rele_button"/>
         <label id="item_id_R7" class="item_value" hidden=true>???</label><div id="item_bit_R7" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;<br><br>
         
        <h2 align="center"> <a href="inicio.jsp">INICIO</a></h2> 
        </div>
        <!--  <div id="mensaje">
        </div>-->
        <div id="alarma">
        <!-- aqui se definen las alarams formato es <label class="alarm_parameter" id="alarm_nombreVariable_tipoLimite_valorLimite" hidden="true"></label> -->
        <!-- ALARMAS -->
        <label class="alarm_parameter" id="alarm_t1_max_26"  hidden=true> <font color="red"> alta temperatura </font></label>
        <label class="alarm_parameter" id="alarm_VDC1_min_44"  hidden=true ><font color="red">bajo voltaje</font></label>
        <label class="alarm_parameter" id="alarm_VDC2_min_44"  hidden=true font color="red">bajo voltaje</label>
        <label class="alarm_parameter" id="alarm_VDC3_min_44"  hidden=true font color="red">bajo voltaje</label>
        <label class="alarm_parameter" id="alarm_VDC4_min_44"  hidden=true font color="red">bajo voltaje</label>
        <label class="alarm_parameter" id="alarm_VAC1_min_90"  hidden=true font color="red">falla de red</label>
        <label class="alarm_parameter" id="alarm_VAC2_min_90"  hidden=true font color="red">falla de red</label>
        <label class="alarm_parameter" id="alarm_VAC3_min_90"  hidden=true font color="red">falla de red</label>       
                </div>
         </body>
</html>