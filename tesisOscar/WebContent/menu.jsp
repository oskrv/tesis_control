<%-- 
    Document   : menu
    Created on : 22/12/2015, 11:07:58 AM
    Author     : Oscar Velasquez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HttpSession objsesion = request.getSession(false);
    String usuario = (String)objsesion.getAttribute("usuario");
    if (usuario.equals("")){
        response.sendRedirect("inicio.jsp");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="StyleSheet" href="css/estilo.css" type="text/css">
       
        <title> SISTEMA DE SUPERVISION y CONTROL</title>
    	<script src="js/jquery.js"></script>
        <script src="js/obtenerVariables.js"></script>
        <script src="js/RegulacionTemperatura.js"></script>
        <script src="js/ManejoRele.js"></script>
        <script src="js/alarmaV2.js"></script>    
        <script src="js/bitdisplay.js"></script>    
    </head>
    <body>
        <h1 align="left">Bienvenido  <% out.println(usuario);%></h1>
        <h1 align="center" >Sistema de Supervision y Control de la Central Merida II</h1>
        <label aling= "Right">Iniciar Sistema</label>
        <input type="button" value="Start"  id=""/>
        
        <div id="indice">
        <h2 align="center"> Temperaturas</h2>
        <div class="monPanel">
        
         <!--  sustituir value en id,por ejemplo item_id_nombreVariable.
         dejar class tal como esta es decir item_value.
          hacer esto para todas las variables con class=item_value!-->
          <!-- <label>PIC termostato</label>-->
        <!--<label id="item_id_E1" class="item_value" hidden=true>???</label><div id="item_bit_E1" class="item_bit_display"  style="height:30px;width:60px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
         -->
        
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><font size="6"><b>Sala Digital:</b></font></label><label id="item_id_t1" class="item_value"><font size="6">???</font></label><br><br>
      	 <label><font size="6">Alarma 1</font></label>
       	<label id="item_id_al1" class="item_value" hidden=true>???</label><div id="item_bit_al1" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
       	 <label><font size="6">Alarma 2</font></label>
         <label id="item_id_al2" class="item_value" hidden=true>???</label><div id="item_bit_al2" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
         <br>
         <br/>Modificar Rango Alto: 
         <select id="select_id_1"><!--aqui va la posicion del comando en el comando.properties, reemplazar solo el 1 -->
	        <option value="1">15°C</option>  <!-- en el parametro value de los options, reemplazarlos por el valor a enviar -->
	        <option value="2">16°C</option>     
	        <option value="3">17°C</option>
			<option value="4">18°C</option>
	  		<option value="5">19°C</option>
	  		<option value="6">20°C</option>
	        <option value="7">21°C</option>
	        <option value="8">22°C</option>
	        <option value="9">23°C</option>
	        <option value="10">24°C</option>
	        <option value="11">25°C</option>
	        <option value="12">26°C</option>
	        <option value="13">27°C</option>
		</select>
		<!-- el numero en el id del buton, debe corresponderse con el select. ej si el select es select_id_2 el boton debe ser boton_id_2 -->
        <br/><button id="boton_id_1" class="botonEnviarOrden">Regular</button><br>
        <!--Repetir para todos los selects de esta area (regulacion de temperaturas)  -->
        <br/>Modificar Rango Bajo: 
        <select id="select_id_2">
	              
	        <option value="1">15°C</option>     
	        <option value="2">16°C</option>     
	        <option value="3">17°C</option>
			<option value="4">18°C</option>
	  		<option value="5">19°C</option>
	  		<option value="6">20°C</option>
	        <option value="7">21°C</option>
	        <option value="8">22°C</option>
	        <option value="9">23°C</option>
	        <option value="10">24°C</option>
	        <option value="11">25°C</option>
	        <option value="12">26°C</option>
	        <option value="13">27°C</option>
		</select>
		 <br/><button id="boton_id_2" class="botonEnviarOrden">Regular</button><br>
         <!--  <label><b>Oficina Movilnet:</b></label><label id="item_id_t1" class="item_value">???</label><br><br>
         <label><b>Sala PCM:</label></b><label id="item_id_t1" class="item_value">???</label><br><br>
         <label><b>Sala Datos:</b></label><label id="item_id_value" class="item_value">???</label><br><br>
         -->
         </div> 
        </div>
       <div id="contenido">
         <h2 align="center"> Monitoreo de Variables Ambientales y Voltajes</h2>
        <div class="monPanel">
        <!--  sustituir value en id,por ejemplo item_id_nombreVariable.
         dejar class tal como esta es decir item_value.
          hacer esto para todas las variables con class=item_value!-->
           <h1 align="center"> Temperaturas </h1>
         &nbsp;&nbsp;&nbsp;<label><b><font color="black" size="5">Temperatura 1:</font></b></label><label id="item_id_temp1" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b><font color="black" size="5">Temperatura 2:</font></b></label><label id="item_id_temp2" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b><font color="black" size="5">Temperatura 3:</font></b></label><label id="item_id_temp3" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b><font color="black" size="5">Temperatura 4:</font></b></label><label id="item_id_temp4" class="item_value">???</label>
         <br><br>
          <h1 align="center"> Voltajes En DC </h1>
         &nbsp;&nbsp;&nbsp;<label><b><font color="black" size="5">Voltaje DC 1:</font></b></label><label id="item_id_VDC1" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b><font color="black" size="5">Voltaje DC 2:</font></b></label><label id="item_id_VDC2" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b><font color="black" size="5">Voltaje DC 3:</font></b></label><label id="item_id_VDC3" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b><font color="black" size="5">Voltaje DC 4:</font></b></label><label id="item_id_VDC4" class="item_value">???</label>
         <br><br>
          <h2 align="center"> Voltajes En AC </h2>
         &nbsp;&nbsp;&nbsp;<label><b><font color="black" size="5">Fase 1:</font></b></label><label id="item_id_VAC1" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b><font color="black" size="5">Fase 2:</font></b></label><label id="item_id_VAC2" class="item_value">???</label>
         &nbsp;&nbsp;&nbsp;<label><b><font color="black" size="5">Fase 3:</font></b></label><label id="item_id_VAC3" class="item_value">???</label>
         </div>
         
         <h2 align="center"> ENTRADAS DIGITALES </h2>
       <!-- el numero que se encuentra justo despues  de "rele_id_" se debe sustituir por el numero de la posicion del comando en el .properties, y luego del "value_"  se debe colocar el valor que se desea enviar al presionar ese boton (Ej:on,off,1,0,)
       por ejemplo
       	id="rele_id_0_value_on"
       	significa que se envia el comando que esta en la posicion 0 y el valor on
       	id="rele_id_0_value_1"
       	significa que se envia el comando que esta en la posicion 0 y el valor 1
      -->
         <label><font color="black" size="5">Dispositivo 1</font></label>
         <label id="item_id_E1" class="item_value" hidden=true>???</label><div id="item_bit_E1" class="item_bit_display"  style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
          <label><font color="black" size="5">Dispositivo 2</font></label>
          <label id="item_id_E2" class="item_value" hidden=true>???</label><div id="item_bit_E2" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
          <label><font color="black" size="5">Dispositivo 3</font></label>
          <label id="item_id_E3" class="item_value" hidden=true>???</label><div id="item_bit_E3" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
          <br><br>
          <label><font color="black" size="5">Dispositivo 4</font></label>
          <label id="item_id_E4" class="item_value" hidden=true>???</label><div id="item_bit_E4" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
          <label><font color="black" size="5">Dispositivo 5</font></label>
          <label id="item_id_E5" class="item_value" hidden=true>???</label><div id="item_bit_E5" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
           <label><font color="black" size="5">Dispositivo 6</font></label>
          <label id="item_id_E6" class="item_value" hidden=true>???</label><div id="item_bit_E6" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
          <br><br>
           <label><font color="black" size="5">Dispositivo 7</font></label>
          <label id="item_id_E7" class="item_value" hidden=true>???</label> <div id="item_bit_E7" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
          <label><font color="black" size="5">Dispositivo 8</font></label>
          <label id="item_id_E8" class="item_value" hidden=true>???</label><div id="item_bit_E8" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
       <h1 align="center"> Control de Reles </h1>
    
        &nbsp;&nbsp;&nbsp;<label><font color="black" size="5">Rele 1</font></label>
        <input type="button" value=" ENCERDER" id="rele_value_S1" 	 class="rele_button"style="height:40px;width:90px";/>
        <input type="button" value=" APAGAR" 	id="rele_value_N1" class="rele_button"style="height:40px;width:90px";/>
         <label id="item_id_R1" class="item_value" hidden=true>???</label><div id="item_bit_R1" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
         <label><font color="black" size="5">Rele 2</font> </label>
         <input type="button" value=" ENCENCER" id="rele_value_S2" class="rele_button"style="height:40px;width:90px"/>
         <input type="button" value=" APAGAR" 	id="rele_value_N2" class="rele_button"style="height:40px;width:90px"/>
         <label id="item_id_R2" class="item_value" hidden=true>???</label><div id="item_bit_R2" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;<br><br>
         &nbsp;&nbsp;&nbsp;<label><font color="black" size="5">Rele 3</font></label>
         <input type="button" value=" ENCENCER" id="rele_value_S3" class="rele_button"style="height:40px;width:90px"/>
         <input type="button" value=" APAGARA" 	id="rele_value_N3" class="rele_button"style="height:40px;width:90px"/>
         <label id="item_id_R3" class="item_value" hidden=true>???</label><div id="item_bit_R3" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
         <label><font color="black" size="5">Rele 4</font></label>
         <input type="button" value=" ENCENCER" id="rele_value_S4" class="rele_button"style="height:40px;width:90px"/>
         <input type="button" value=" APAGAR" 	id="rele_value_N4" class="rele_button"style="height:40px;width:90px"/>
         <label id="item_id_R4" class="item_value" hidden=true>???</label><div id="item_bit_R4" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;<br><br>
         &nbsp;&nbsp;&nbsp;<label><font color="black" size="5">Rele 5</font></label>
         <input type="button" value=" ENCENCER" id="rele_value_S5" class="rele_button"style="height:40px;width:90px"/>
         <input type="button" value=" APAGARr" 	id="rele_value_N5" class="rele_button"style="height:40px;width:90px"/>
         <label id="item_id_R5" class="item_value" hidden=true>???</label><div id="item_bit_R5" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;
         <label><font color="black" size="5">Rele 6</font></label>
         <input type="button" value=" ENCENCER" id="rele_value_S6" class="rele_button"style="height:40px;width:90px"/>
         <input type="button" value=" APAGAR" 	id="rele_value_N6" class="rele_button"style="height:40px;width:90px"/>
         <label id="item_id_R6" class="item_value" hidden=true>???</label><div id="item_bit_R6" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;<br><br>
         &nbsp;&nbsp;&nbsp;<label><font color="black" size="5">Rele 7</font></label>
         <input type="button" value=" ENCENCER" id="rele_value_S7" class="rele_button"style="height:40px;width:90px"/>
         <input type="button" value=" APAGAR" 	id="rele_value_N7" class="rele_button"style="height:40px;width:90px"/>
         <label id="item_id_R7" class="item_value" hidden=true>???</label><div id="item_bit_R7" class="item_bit_display" style="height:30px;width:50px;display:inline-block"></div>&nbsp;&nbsp;&nbsp;<br><br>
         <h2 align="center"><a href="">HISTORIAL</a></h2>
         <h2 align="center"> <a href="menu2.jsp">ATRAS</a></h2> 
         <h2 align="center"> <a href="inicio.jsp">INICIO</a></h2> 
        </div>
        <!--  <div id="mensaje">
        </div>-->
        <div id="alarma">
        <!-- aqui se definen las alarams formato es <label class="alarm_parameter" id="alarm_nombreVariable_tipoLimite_valorLimite" hidden="true"></label> -->
        <!-- ALARMAS -->
        <label class="alarm_parameter" id="alarm_t1_max_26"  hidden=true> <font color="red"> alta temperatura </font></label>
        <label class="alarm_parameter" id="alarm_VDC1_min_44"  hidden=true ><font color="red">bajo voltaje</font></label>
        <label class="alarm_parameter" id="alarm_VDC2_min_44"  hidden=true font color="red">bajo voltaje</label>
        <label class="alarm_parameter" id="alarm_VDC3_min_44"  hidden=true font color="red">bajo voltaje</label>
        <label class="alarm_parameter" id="alarm_VDC4_min_44"  hidden=true font color="red">bajo voltaje</label>
        <label class="alarm_parameter" id="alarm_VAC1_min_90"  hidden=true font color="red">falla de red</label>
        <label class="alarm_parameter" id="alarm_VAC2_min_90"  hidden=true font color="red">falla de red</label>
        <label class="alarm_parameter" id="alarm_VAC3_min_90"  hidden=true font color="red">falla de red</label>       
                </div>
         </body>
</html>
